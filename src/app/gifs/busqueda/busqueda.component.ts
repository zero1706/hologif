import { Component, ElementRef, ViewChild} from '@angular/core';
import { GifsService } from '../services/gifs.service';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.css']
})
export class BusquedaComponent  {
@ViewChild('txtBuscar') txtbuscar!:ElementRef<HTMLInputElement>;// operador !: que asegura que el objeto no es nulo

  constructor(private gifsService: GifsService){}
  busqueda(){
    const valor = this.txtbuscar.nativeElement.value;
    if(valor.trim().length ===0){
      return;
    }
    this.gifsService.buscarGifs(valor);
    this.txtbuscar.nativeElement.value='';
  }


}
